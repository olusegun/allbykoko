<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package allbykoko
 */
//* Template Name: Homepage
get_header(); ?>
	<div class="slider">
        <div class="kokoslider">
        	<img src="<?php echo get_theme_mod('slider_img'); ?>" class="img-responsive" alt="lady image">
        </div>
    </div>
</div><!-- #masthead -->

 <nav class="navbar" role="navigation">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="fa fa-2x">Menu </span>
        <span class="fa fa-bars fa-2x"></span>
      </button>
    </div>
        <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 1,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'col-md-3 col-sm-3 col-xs-12',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
    </div>
    </nav><!-- #site-navigation -->
	
	<div class="noleft">
    <div class="fashion" style="background-image:url('<?php echo get_theme_mod('fashion_bgimg'); ?>');">
        <div class="imdiv">
            <h2 class="boxtitle"><?php echo get_theme_mod('fashion_title'); ?></h2>
            <p class="boxtext"><?php echo get_theme_mod('fashion_text'); ?></p>
            <?php if(get_theme_mod('fashion_url')){ ?>
            <p class="boxtext"><a href="<?php echo get_theme_mod('fashion_url'); ?>">...Read more</a></p>
        	<?php  } ?>
        </div>
    </div>
    <div class="hmdecor" style="background-image:url('<?php echo get_theme_mod('decor_bgimg'); ?>');">
        <div class="imdiv">
            <h2 class="boxtitle"><?php echo get_theme_mod('decor_title'); ?></h2>
            <p class="boxtext"><?php echo get_theme_mod('decor_text'); ?></p>
            <?php if(get_theme_mod('decor_url')){ ?>
            <p class="boxtext"><a href="<?php echo get_theme_mod('decor_url'); ?>">...Read more</a></p>
           <?php  } ?>
        </div>
    </div>
    <div class="healthytray" style="background-image:url('<?php echo get_theme_mod('healthytray_bgimg'); ?>');">
        <div class="imdiv">
            <h2 class="boxtitle"><?php echo get_theme_mod('healthytray_title'); ?></h2>
            <p class="boxtext"><?php echo get_theme_mod('healthytray_text'); ?></p>
            <?php if(get_theme_mod('healthytray_url')){ ?>
            <p class="boxtext"><a href="<?php echo get_theme_mod('healthytray_url'); ?>">...Read more</a></p>
           <?php  } ?>
        </div>
    </div>
</div>

<div class="container">
<div class="row">
    <div class="col-md-6">
    <img src="<?php echo get_theme_mod('aboutkoko_img'); ?>" alt="Ifueko" class="img-responsive center-block">
    </div>
    <div class="col-md-6">
    <h1 class="abouttitle"><?php echo get_theme_mod('aboutkoko_title'); ?></h1>
    <?php  echo get_theme_mod('aboutkoko_text'); ?>
    </div>
</div>
</div>

<?php
get_footer();