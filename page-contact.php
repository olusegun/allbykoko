<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package allbykoko
 */
//* Template Name: Contact
get_header(); ?>
</div><!-- #masthead -->
	<nav class="navbar kokomenu text-center" role="navigation">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="fa fa-2x">Menu </span>
        <span class="fa fa-bars fa-2x"></span>
      </button>
    </div>

        <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 1,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'col-md-3 col-sm-3 col-xs-12',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
</div>
</nav><!-- #site-navigation -->

<?php
			while ( have_posts() ) : the_post(); ?>

	<div class="container">
    <hr class="styled">
	</div>

	<div class="container">
    <div class="col-md-6 col-xs-11 text-center">
    <?php the_title( '<h1 class="kokohead">', '</h1>' ); ?>
 	<?php the_content(); ?>
<?php
 	if (get_post_meta($post->ID, "meta-box-text-tel", true)){ 
 	?>
        <b>
            <p><i class="fa fa-phone fa-lg" aria-hidden="true"></i> <?php echo get_post_meta($post->ID, "meta-box-text-tel", true); ?></p>
            <p><i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i><?php echo get_post_meta($post->ID, "meta-box-text-whatsapp", true); ?></p>
            <p><i class="fa fa-envelope fa-lg" aria-hidden="true"> </i><?php echo get_post_meta($post->ID, "meta-box-text-email", true); ?></p>
            <p><i class="fa fa-instagram fa-lg" aria-hidden="true"> </i><?php echo get_post_meta($post->ID, "meta-box-text-instagram", true); ?></p>
            <p></p>
        </b>
<?php } ?>
    </div>
    <div class="form col-md-6 col-xs-11">
    <?php 
   echo do_shortcode(  get_post_meta($post->ID, "meta-box-contact-shortcode", true) );

       ?>
    </div>
</div>
<?php
endwhile; // End of the loop.
			?>	
<?php
get_footer();
