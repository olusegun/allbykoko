<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package allbykoko
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<title>All By Koko... Coming Soon</title>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link href="https://fonts.googleapis.com/css?family=Dancing+Script%7CYellowtail" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cabin%7CPlayfair+Display" rel="stylesheet">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="noleft">
		<div>
			<h1 class="koko">
                    <?php if(get_theme_mod('nf_logo')):?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name','display' ); ?>" rel="home">
                     <img class="img-responsive" src="<?php echo get_theme_mod('nf_logo'); ?>" alt="<?php bloginfo( 'name','display' ); ?>" ></a> 
                    <?php else: ?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                    <?php
                    $description = get_bloginfo( 'description', 'display' );
                    if ( $description || is_customize_preview() ) : ?>
                        <small itemprop="description" class="site-description text-muted" style="display:none;"><?php echo $description; /* WPCS: xss ok. */ ?></small>
                    <?php
                    endif; ?>
          
        <?php endif; ?>
        </h1>        
    </div><!-- .site-branding -->