<?php
/**
 * allbykoko functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package allbykoko
 */

if ( ! function_exists( 'koko_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function koko_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on allbykoko, use a find and replace
	 * to change 'koko' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'koko', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	add_image_size('featured_preview', 55, 55, true);

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'koko' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'koko_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'koko_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function koko_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'koko_content_width', 640 );
}
add_action( 'after_setup_theme', 'koko_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function koko_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'koko' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'koko' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'koko_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function koko_scripts() {
	wp_enqueue_script( 'koko-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'koko-script', get_template_directory_uri() . '/js/script.js', array(), '20151215', false );

	wp_enqueue_script( 'koko-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'Bootstrap_script', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

function koko_styles(){
	wp_enqueue_style( 'koko-style', get_stylesheet_uri() );
	wp_enqueue_style( 'Font_Awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
	wp_enqueue_style( 'Bootstrap_css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css' );
    wp_enqueue_style( 'Style', get_template_directory_uri() . '/css/style.css' );
    wp_enqueue_style('litebx', get_template_directory_uri() . '/css/litebx.css');  
    wp_enqueue_style( 'font-style', get_template_directory_uri() . '/css/ie10-viewport-bug-workaround.css');
}


add_action( 'wp_enqueue_scripts', 'koko_scripts' );
add_action( 'wp_enqueue_scripts', 'koko_styles' );



function koko_paragraph($content){
    return preg_replace('/<p([^>]+)?>/', '<p class="fullp koktext">', $content);
}
add_filter('the_content', 'koko_paragraph');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/koko-testimonial.php';
require_once get_template_directory() . '/inc/gallery-metabox/gallery.php';
require get_template_directory() . '/inc/koko-meal-recipe.php';
require get_template_directory() . '/inc/koko_page_metaboxes.php';
require_once get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
/**
 * Theme-plugin-recommended
 */
require get_template_directory(). '/inc/theme-plugin.php';