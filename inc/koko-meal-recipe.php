<?php
/*
// Koko recipe

*/

add_action( 'init', 'create_custom_recipe' );
function create_custom_recipe() {
    register_post_type( 'recipe',
        array(
            'labels' => array(
                'name' => 'Koko Meal Recipe',
                'singular_name' => 'recipe',
                'add_new' => 'New Recipe',
                'add_new_item' => 'New Recipe',
                'edit' => 'Edit',
                'edit_item' => 'Edit Recipe',
                'new_item' => 'New ',
                'view' => 'View',
                'view_item' => 'View Recipe',
                'search_items' => 'Search Recipe',
                'not_found' => 'No recipe found',
                'not_found_in_trash' => 'No recipe found in Trash',
                'parent' => 'Parent recipe'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'editor','thumbnail','title', 'comments'),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "recipe")
        )
    );
}

// GET FEATURED IMAGE
add_filter('manage_recipe_posts_columns', 'recipe_columns', 5);
add_action('manage_recipe_posts_custom_column', 'posts_recipe_columns', 5, 2);
function recipe_columns($defaults){
    $defaults['riv_post_thumbs'] = __('Thumbs');
    return $defaults;
}
function posts_recipe_columns($column_name, $id){
        if($column_name === 'riv_post_thumbs'){
        echo the_post_thumbnail( 'thumbnail','img-responsive' );
    }
}