<?php
/*
// Koko Testimonial

*/

add_action( 'init', 'create_custom_testimonial' );
function create_custom_testimonial() {
    register_post_type( 'testimonial',
        array(
            'labels' => array(
                'name' => 'Koko Testimonial',
                'singular_name' => 'Testimonial',
                'add_new' => 'New Testimonial',
                'add_new_item' => 'New Testimonial',
                'edit' => 'Edit',
                'edit_item' => 'Edit Testimonial',
                'new_item' => 'New Testimonial',
                'view' => 'View',
                'view_item' => 'View Testimonial',
                'search_items' => 'Search Testimonial',
                'not_found' => 'No Testimonial found',
                'not_found_in_trash' => 'No Testimonial found in Trash',
                'parent' => 'Parent Testimonial'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'editor','title'),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "testimonial")
        )
    );
}

function wpb_change_title_text( $title ){
     $screen = get_current_screen();
  
     if  ( 'testimonial' == $screen->post_type ) {
          $title = 'Enter Reviewer Name';
     }
  
     return $title;
}
  
add_filter( 'enter_title_here', 'wpb_change_title_text' );

add_filter('manage_testimonial_posts_columns', 'koko_replace_column_title');
function koko_replace_column_title( $posts_columns ) {
    
    $posts_columns[ 'title' ] = 'Reviewer';
    return $posts_columns;
}

add_filter('manage_testimonial_posts_columns', 'posts_columns', 5);
add_action('manage_testimonial_posts_custom_column', 'posts_custom_columns', 5, 2);
function posts_columns($defaults){
    $defaults['testimonial_content'] = __('Content');
    return $defaults;
}
function posts_custom_columns($column_name, $id){
        if($column_name === 'testimonial_content'){
        echo the_content();
    }
}