<?php
/*

// Koko Meta box 

*/

// Save the Metabox Data


function page_design_add_meta_boxes_page($post) {
        
    if ( 'page-koko.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {

    add_meta_box('koko_contact', 'Contact Form Section', 'koko_contact', 'page', 'normal', 'high');
    
    } 
    if ('page-contact.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ){
    add_meta_box('koko_contact_address', 'Contact Address', 'koko_contact_address', 'page', 'normal', 'high');
    add_meta_box('koko_contact', 'Contact Form Section', 'koko_contact', 'page', 'normal', 'high');

    }
    if('page-about.php' == get_post_meta($post->ID, '_wp_page_template', true)){
        add_meta_box('koko_about_page', 'Koko Brands', 'koko_about_page', 'page', 'normal', 'high');
        add_meta_box('koko_contact', 'Contact Form Section', 'koko_contact', 'page', 'normal', 'high');

    }

    if('page-healthy.php' == get_post_meta($post->ID, '_wp_page_template', true)){
        add_meta_box('koko_page_design1', 'Testimonial', 'koko_page_design1', 'page', 'normal', 'high');
        add_meta_box('koko_recipe_meals', 'Meals', 'koko_recipe_meal', 'page', 'normal', 'high');

    }
    if('page-recipe.php' == get_post_meta($post->ID, '_wp_page_template', true)){
    add_meta_box('koko_contact', 'Contact Form Section', 'koko_contact', 'page', 'normal', 'high');

    }

    if('page-supportgroup.php' == get_post_meta($post->ID, '_wp_page_template', true)){
    add_meta_box('koko_contact', 'Contact Form Section', 'koko_contact', 'page', 'normal', 'high');

    }
}
add_action( 'add_meta_boxes_page', 'page_design_add_meta_boxes_page' );


// recipe 
function koko_recipe_meal($post){

    $url = home_url( '/wp-admin/post-new.php?post_type=recipe');
    wp_nonce_field(basename(__FILE__), "meta-box-nonce"); ?>
   <p>add recipe image <a href="<?php echo $url; ?>">here</a></p>
    <p>
     <input name="meta-box-recipe-link" type="url" placeholder="Click for recipe link" value="<?php echo get_post_meta($post->ID, "meta-box-recipe-link", true); ?>">
     </p>
    <p>
<?php    
}

// about page Koko Brand

function koko_about_page($post) {
    
   wp_nonce_field(basename(__FILE__), "meta-box-nonce"); ?>
   
    <div style="border: 1px solid #c1c1c1; width: 70%; padding: 5px; margin: 5px;">
    <b>Brand 1</b>
    <p>
     <input name="meta-box-brand-title1" type="text" placeholder="Title" value="<?php echo get_post_meta($post->ID, "meta-box-brand-title1", true); ?>">
     </p>
    <p>
     <input name="meta-box-brand-link1" type="url" placeholder="Brand link" value="<?php echo get_post_meta($post->ID, "meta-box-brand-link1", true); ?>">
     </p>
     
     <p>
     <input name="meta-box-brand-img1" type="url" placeholder="Image url" value="<?php echo get_post_meta($post->ID, "meta-box-brand-img1", true); ?>">
     </p>

    </div>

    <div style="border: 1px solid #c1c1c1; width: 70%; padding: 5px; margin: 5px;">
    <b>Brand 2</b>
    <p>
     <input name="meta-box-brand-title2" type="text" placeholder="Title" value="<?php echo get_post_meta($post->ID, "meta-box-brand-title2", true); ?>">
     </p>
    <p>
     <input name="meta-box-brand-link2" type="url" placeholder="brand link http://" value="<?php echo get_post_meta($post->ID, "meta-box-brand-link2", true); ?>">
     </p>
     
     <p>
     <input name="meta-box-brand-img2" type="url" placeholder="image url" value="<?php echo get_post_meta($post->ID, "meta-box-brand-img2", true); ?>">
     </p>

    </div>

    <div style="border: 1px solid #c1c1c1; width: 70%; padding: 5px; margin: 5px;">
    <b>Brand 3</b>
    <p>
     <input name="meta-box-brand-title3" type="text" placeholder="Title" value="<?php echo get_post_meta($post->ID, "meta-box-brand-title3", true); ?>">
     </p>
    <p>
     <input name="meta-box-brand-link3" type="url" placeholder="brand link http://" value="<?php echo get_post_meta($post->ID, "meta-box-brand-link3", true); ?>">
     </p>
     
     <p>
     <input name="meta-box-brand-img3" type="url" placeholder="image url" value="<?php echo get_post_meta($post->ID, "meta-box-brand-img3", true); ?>">
     </p>

    </div>
    <?php
}

//contact page address

function koko_contact_address($post) {
    
   wp_nonce_field(basename(__FILE__), "meta-box-nonce");
    ?>
     <p>
     <h4>Phone</h4>
     <input name="meta-box-text-tel" type="text" value="<?php echo get_post_meta($post->ID, "meta-box-text-tel", true); ?>">
     </p>
     <p>
     <h4>WhatsApp</h4>
     <input name="meta-box-text-whatsapp" type="text" value="<?php echo get_post_meta($post->ID, "meta-box-text-whatsapp", true); ?>">
     </p>
     <p>
     <h4>Email</h4>
     <input name="meta-box-text-email" type="email" value="<?php echo get_post_meta($post->ID, "meta-box-text-email", true); ?>">
     </p>
     <p>
     <h4>Instagram</h4>
     <input name="meta-box-text-instagram" type="text" value="<?php echo get_post_meta($post->ID, "meta-box-text-instagram", true); ?>">
     </p>
    <?php
}

function koko_page_design1($post) {
    $url = home_url( '/wp-admin/post-new.php?post_type=testimonial');
    wp_nonce_field(basename(__FILE__), "meta-box-nonce"); ?>
   <p>add new testimonial <a href="<?php echo $url; ?>">here</a></p>
    
    <p><label for="meta-box-checkbox1">Show Testimonial on this page</label>
            <?php
                $checkbox_value1 = get_post_meta($post->ID, "meta-box-checkbox1", true);

                if($checkbox_value1 == "")
                {
                    ?>
                        <input name="meta-box-checkbox1" type="checkbox" value="true">
                    <?php
                }
                else if($checkbox_value1 == "true")
                {
                    ?>  
                        <input name="meta-box-checkbox1" type="checkbox" value="true" checked>
                    <?php
                }
            ?>
        </p>
   <?php

}



function koko_contact($post) {
    
   wp_nonce_field(basename(__FILE__), "meta-box-nonce");
    ?>
     <p>
     <h4>Shortcode</h4>
     <textarea cols="50" name="meta-box-contact-shortcode"> <?php echo get_post_meta($post->ID, "meta-box-contact-shortcode", true); ?></textarea>
    </p>
    <?php
}



function save_custom_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;
       

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "page";
    if($slug != $post->post_type)
        return $post_id;

    //conact section
    
    $meta_box_textarea_value = "";
    
    //testimonial
    $meta_box_checkbox_value1 = "";

    
    //address
    $meta_box_tel_value = "";
    $meta_box_whatsapp_value = "";
    $meta_box_email_value = "";
    $meta_box_instagram_value = "";

    //koko brand

    $meta_box_title1 = "";
    $meta_box_link1 = "";
    $meta_box_img1 = "";

    $meta_box_title2 = "";
    $meta_box_link2 = "";
    $meta_box_img2 = "";

    $meta_box_title3 = "";
    $meta_box_link3 = "";
    $meta_box_img3 = "";

    $meta_box_recipe_link = "";

   

   if(isset($_POST["meta-box-recipe-link"]))
    {
        $meta_box_recipe_link = $_POST["meta-box-recipe-link"];
    }   
    update_post_meta($post_id, "meta-box-recipe-link", $meta_box_recipe_link);


    if(isset($_POST["meta-box-contact-shortcode"]))
    {
        $meta_box_textarea_value = $_POST["meta-box-contact-shortcode"];
    }   
    update_post_meta($post_id, "meta-box-contact-shortcode", $meta_box_textarea_value);


    if(isset($_POST["meta-box-checkbox1"]))
    {
        $meta_box_checkbox_value1 = $_POST["meta-box-checkbox1"];
    }   
    update_post_meta($post_id, "meta-box-checkbox1", $meta_box_checkbox_value1);


    //address

    if(isset($_POST["meta-box-text-tel"]))
    {
        $meta_box_tel_value = $_POST["meta-box-text-tel"];
    }   
    update_post_meta($post_id, "meta-box-text-tel", $meta_box_tel_value);

    if(isset($_POST["meta-box-text-email"]))
    {
        $meta_box_email_value = $_POST["meta-box-text-email"];
    }   
    update_post_meta($post_id, "meta-box-text-email", $meta_box_email_value);
    
    if(isset($_POST["meta-box-text-whatsapp"]))
    {
        $meta_box_whatsapp_value = $_POST["meta-box-text-whatsapp"];
    }   
    update_post_meta($post_id, "meta-box-text-whatsapp", $meta_box_whatsapp_value);

    if(isset($_POST["meta-box-text-instagram"]))
    {
        $meta_box_instagram_value = $_POST["meta-box-text-instagram"];
    }   
    update_post_meta($post_id, "meta-box-text-instagram", $meta_box_instagram_value);


    //koko brand
    if (isset($_POST["meta-box-brand-title1"])){
        $meta_box_title1 = $_POST["meta-box-brand-title1"];
    }
        update_post_meta($post_id, "meta-box-brand-title1", $meta_box_title1);

    if (isset($_POST["meta-box-brand-link1"])){
        $meta_box_link1 = $_POST["meta-box-brand-link1"];
    }
        update_post_meta($post_id, "meta-box-brand-link1", $meta_box_link1);

    if (isset($_POST["meta-box-brand-img1"])){
        $meta_box_img1 = $_POST["meta-box-brand-img1"];
    }
        update_post_meta($post_id, "meta-box-brand-img1", $meta_box_img1);

     if (isset($_POST["meta-box-brand-title2"])){
        $meta_box_title2 = $_POST["meta-box-brand-title2"];
    }
        update_post_meta($post_id, "meta-box-brand-title2", $meta_box_title2);

    if (isset($_POST["meta-box-brand-link2"])){
        $meta_box_link2 = $_POST["meta-box-brand-link2"];
    }
        update_post_meta($post_id, "meta-box-brand-link2", $meta_box_link2);

    if (isset($_POST["meta-box-brand-img2"])){
        $meta_box_img2 = $_POST["meta-box-brand-img2"];
    }
        update_post_meta($post_id, "meta-box-brand-img2", $meta_box_img2);

     if (isset($_POST["meta-box-brand-title3"])){
        $meta_box_title3 = $_POST["meta-box-brand-title3"];
    }
        update_post_meta($post_id, "meta-box-brand-title3", $meta_box_title3);

    if (isset($_POST["meta-box-brand-link3"])){
        $meta_box_link3 = $_POST["meta-box-brand-link3"];
    }
        update_post_meta($post_id, "meta-box-brand-link3", $meta_box_link3);

    if (isset($_POST["meta-box-brand-img3"])){
        $meta_box_img3 = $_POST["meta-box-brand-img3"];
    }
        update_post_meta($post_id, "meta-box-brand-img3", $meta_box_img3);

}

add_action("save_post", "save_custom_meta_box", 10, 3);

