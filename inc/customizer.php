<?php
/**
 * allbykoko Theme Customizer.
 *
 * @package allbykoko
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function koko_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';



//diable section header image
$wp_customize->remove_section('header_image');
			$wp_customize->remove_section('static_front_page');
			$wp_customize->remove_control('header_textcolor');
			$wp_customize->remove_control('background_color');
			//$wp_customize->remove_section('background_image');

	$wp_customize->add_setting('nf_logo',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'nf_logo', array(

			'label'		=>	_('Upload Logo to replace site name'),
			'section'	=>	'title_tagline',
			'settings'	=>	'nf_logo',
			)));


	$wp_customize->add_setting('slider_img',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

	
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'slider_img', array(

			'label'		=>	_('Upload slider image,(min: 1309px x400px)'),
			'section'	=>	'home_slider',
			'settings'	=>	'slider_img',
			)));


	//Koko Fashion
	$wp_customize->add_setting('fashion_title', 
			array('default' => '',
            	  'sanitize_callback' =>'sanitize_text_field'
			 ));

			$wp_customize->add_control('fashion_title', array(

			'label'		=>	_('Box Title'),
			'section'	=>	'home_section1',
			'settings'	=>	'fashion_title',
			'type'		=>	'textbox',
			));

	$wp_customize->add_setting('fashion_text', 
			array('default' => '',
            	  'sanitize_callback' =>'sanitize_text_field'
			 ));

			$wp_customize->add_control('fashion_text', array(

			'label'		=>	_('Box Text'),
			'section'	=>	'home_section1',
			'settings'	=>	'fashion_text',
			'type'		=>	'textarea',
			));

	$wp_customize->add_setting('fashion_url', 
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

			$wp_customize->add_control('fashion_url', array(

			'label'		=>	_('Read more url'),
			'section'	=>	'home_section1',
			'settings'	=>	'fashion_url',
			'type'		=>	'url',
			));

	$wp_customize->add_setting('fashion_bgimg',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

	
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'fashion_bgimg', array(

			'label'		=>	_('Background image(min: 1309px x400px)'),
			'section'	=>	'home_section1',
			'settings'	=>	'fashion_bgimg',
			)));

	//End of fashion section


	//Koko Home decor
	$wp_customize->add_setting('decor_title', 
			array('default' => '',
            	  'sanitize_callback' =>'sanitize_text_field'
			 ));

			$wp_customize->add_control('decor_title', array(

			'label'		=>	_('Box Title'),
			'section'	=>	'home_section2',
			'settings'	=>	'decor_title',
			'type'		=>	'textbox',
			));

	$wp_customize->add_setting('decor_text', 
			array('default' => '',
            	  'sanitize_callback' =>'sanitize_text_field'
			 ));

			$wp_customize->add_control('decor_text', array(

			'label'		=>	_('Box Text'),
			'section'	=>	'home_section2',
			'settings'	=>	'decor_text',
			'type'		=>	'textarea',
			));

	$wp_customize->add_setting('decor_url', 
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

			$wp_customize->add_control('decor_url', array(

			'label'		=>	_('Read more Url'),
			'section'	=>	'home_section2',
			'settings'	=>	'decor_url',
			'type'		=>	'url',
			));

	$wp_customize->add_setting('decor_bgimg',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

	
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'decor_bgimg', array(

			'label'		=>	_('Background image(min: 1309px x400px)'),
			'section'	=>	'home_section2',
			'settings'	=>	'decor_bgimg',
			)));

		// end of home decor

		/**
		* Add Koko Panel to customizer
		*/


	// Koko healthytray

	$wp_customize->add_setting('healthytray_title', 
			array('default' => '',
            	  'sanitize_callback' =>'sanitize_text_field'
			 ));

			$wp_customize->add_control('healthytray_title', array(

			'label'		=>	_('Box Title'),
			'section'	=>	'home_section3',
			'settings'	=>	'healthytray_title',
			'type'		=>	'textbox',
			));

	$wp_customize->add_setting('healthytray_text', 
			array('default' => '',
            	  'sanitize_callback' =>'sanitize_text_field'
			 ));

			$wp_customize->add_control('healthytray_text', array(

			'label'		=>	_('Box Text'),
			'section'	=>	'home_section3',
			'settings'	=>	'healthytray_text',
			'type'		=>	'textarea',
			));

	$wp_customize->add_setting('healthytray_url', 
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

			$wp_customize->add_control('healthytray_url', array(

			'label'		=>	_('Read more Url'),
			'section'	=>	'home_section3',
			'settings'	=>	'healthytray_url',
			'type'		=>	'url',
			));

	$wp_customize->add_setting('healthytray_bgimg',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

	
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'healthytray_bgimg', array(

			'label'		=>	_('Background image(min: 1309px x400px)'),
			'section'	=>	'home_section3',
			'settings'	=>	'healthytray_bgimg',
			)));

		// end of healthytray

		
	// Koko aboutkoko

	$wp_customize->add_setting('aboutkoko_title', 
			array('default' => '',
            	  'sanitize_callback' =>'sanitize_text_field'
			 ));

			$wp_customize->add_control('aboutkoko_title', array(

			'label'		=>	_('Box Title'),
			'section'	=>	'home_section4',
			'settings'	=>	'aboutkoko_title',
			'type'		=>	'textbox',
			));

	$wp_customize->add_setting('aboutkoko_text', array());

			$wp_customize->add_control('aboutkoko_text', array(

			'label'		=>	_('Box Text (Put text in <p>...</p>)'),
			'section'	=>	'home_section4',
			'settings'	=>	'aboutkoko_text',
			'type'		=>   'textarea',
			) );

	$wp_customize->add_setting('aboutkoko_img',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

	
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'aboutkoko_img', array(

			'label'		=>	_('About image(min: 1309px x400px)'),
			'section'	=>	'home_section4',
			'settings'	=>	'aboutkoko_img',
			)));

		// end of aboutkoko

		$wp_customize->add_panel('theme-options', array(
	    'title' => __( 'Theme settings' , 'koko' ),
	      'description'=> __( 'Koko site options.' , 'koko' ),
	      'priority' => 130
	   		));
		
			
			
			$wp_customize->add_section('home_slider', array(
			'title'		=>_('Home Slider'),
			'Priority'	=> 20,
			'panel' => 'theme-options',

			));

			$wp_customize->add_section('home_section1', array(
			'title'		=>_('Home Section 1 (Fashion)'),
			'Priority'	=> 20,
			'panel' => 'theme-options',

			));

			$wp_customize->add_section('home_section2', array(
			'title'		=>_('Home Section 2 (Home Decor)'),
			'Priority'	=> 20,
			'panel' => 'theme-options',

			));

			$wp_customize->add_section('home_section3', array(
			'title'		=>_('Home Section 3 (healthytray)'),
			'Priority'	=> 20,
			'panel' => 'theme-options',

			));

			$wp_customize->add_section('home_section4', array(
			'title'		=>_('Home Section 4 (About Koko)'),
			'Priority'	=> 20,
			'panel' => 'theme-options',

			));

}
add_action( 'customize_register', 'koko_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function koko_customize_preview_js() {
	wp_enqueue_script( 'koko_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'koko_customize_preview_js' );
