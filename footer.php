<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package allbykoko
 */

?>


<footer>
    <div class="container-fluid footer">
        <div class="container">
            <div class="col-md-3">
                <p><a href="index.html">All by Koko</a>&nbsp;2016</p>
            </div>
            <div class="col-md-3 col-xs-6">
                <p><a href="about.html">About Koko</a></p>
                <p><a href="fashion.html">Koko by Koko</a></p>
                <p><a href="decor.html"> Create My Space</a></p>
                <p><a href="healthytrays.html">Healthy Trays</a></p>
            </div>
            <div class="col-md-3 col-xs-6">
                <p><a href="contact.html">Contact Us</a></p>
                <p>+234 080 123 4567</p>
                <p>
                    <a href="mailto:info@allbykoko.com">
                        info@allbykoko.com
                    </a>
                </p>
            </div>
            <div class="col-md-3">
                <p>Social Media</p>
                <div class="col-xs-4"><a href="#" class="fa fa-instagram fa-2x"></a></div>
                <div class="col-xs-4"><a href="#" class="fa fa-twitter fa-2x"></a></div>
                <div class="col-xs-4"><a href="#" class="fa fa-facebook fa-2x"></a></div>
                
            </div>
        </div>
        <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'koko' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'koko' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme Developed: %1$s by %2$s.', 'koko' ), 'koko', '<a href="#" rel="designer">Olusegun</a>' ); ?>
    </div>

</footer>
<?php wp_footer(); ?>
</body>
</html>