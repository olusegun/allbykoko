<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package allbykoko
 */
//* Template Name: Healthy Tray
get_header(); ?>
</div><!-- #masthead -->
	<nav class="navbar kokomenu text-center" role="navigation">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="fa fa-2x">Menu </span>
        <span class="fa fa-bars fa-2x"></span>
      </button>
    </div>

        <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 1,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'col-md-3 col-sm-3 col-xs-12',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
</div>
</nav><!-- #site-navigation -->

	<?php
			while ( have_posts() ) : the_post();

			?>

			<div class="container-fluid">
    <hr class="styled">
</div>

<div class="slider no-gutter">
    <div class="col-md-6">
    <?php  the_post_thumbnail( 'large', array( 'class' => 'img-responsive center-block' ) ); ?>
    </div>
    <div class="col-md-6">
    	<?php the_title( '<h1 class="abouttitle koktext">', '</h1>' ); ?>
    	<?php the_content(); ?>
    </div>
</div>


<!--  Testimonial section -->
<?php
//show testimonial
if (get_post_meta($post->ID, "meta-box-checkbox1", true)){ 

        $the_query = new WP_Query( array(
            
            'orderby'           => 'date',
            'order'             => 'DESC',
            'posts_per_page'    => 3,
            'post_type'         => 'testimonial',
            'post_status' => 'publish',
            'ignore_sticky_posts' => false,
                        
        ));
    

    if ( $the_query->have_posts() ) { ?>
                <div class="container-fluid sometop">
                <hr class="styled">
                </div>
                <div class="container">
                <h1 class="kokohead">Testimonials</h1>
               <?php
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    echo '<div class="col-md-4">';
                    echo '<blockquote>';
                    the_content();
                    echo '<footer>';
                    the_title() ;
                    echo "</footer></blockquote></div>";
                }
                /* Restore original Post Data */
                wp_reset_postdata(); ?>
                </div>
    <?php } 

    
}

if (get_post_meta($post->ID, "meta-box-recipe-link", true)){ 
    $url = get_post_meta($post->ID, "meta-box-recipe-link", true);
        $the_query2 = new WP_Query( array(
            
            'orderby'           => 'date',
            'order'             => 'DESC',
            'posts_per_page'    => 8,
            'post_type'         => 'recipe',
            'post_status' => 'publish',
            'ignore_sticky_posts' => false,
                        
        ));
    
        if ( $the_query2->have_posts() ) { ?>
                <div class="container sometop">
    			<hr class="styled">
				</div>

				<div class="container somebottom">
    			<h1 class="kokohead">Meals</h1>
    			<div>
               <?php
                while ( $the_query2->have_posts() ) {
                    $the_query2->the_post(); 
                    echo '<div class="col-md-2"><a href="' .$url. '">';
                    
                    the_post_thumbnail( 'large', array( 'class' => 'img-responsive center-block' ) );
                    echo '<p class="text-center">Click for recipe</p></a></div>';
                    
                }
                /* Restore original Post Data */
                wp_reset_postdata(); ?>
                </div>
                </div>
    <?php } 

    }?>    
    
<a href=""></a>
	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'koko' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	   
			<?php 
            endif;
            endwhile; // End of the loop.
			?>
<?php
get_footer();
