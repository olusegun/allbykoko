<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package allbykoko
 */
//* Template Name: About
get_header(); ?>
</div><!-- #masthead -->
	<nav class="navbar kokomenu text-center" role="navigation">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="fa fa-2x">Menu </span>
        <span class="fa fa-bars fa-2x"></span>
      </button>
    </div>

        <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 1,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'col-md-3 col-sm-3 col-xs-12',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
</div>
</nav><!-- #site-navigation -->

	<?php
			while ( have_posts() ) : the_post(); ?>

			<div class="container">
    		<hr class="styled">
			</div>

			<div class="container no-gutter">
    			<div class="col-md-6">
    		<?php  the_post_thumbnail( 'large', array( 'class' => 'img-responsive center-block' ) ); ?>
			</div>
    <div class="col-md-6">
        <?php the_title( '<h1 class="abouttitle koktext">', '</h1>' ); ?>
    	<?php the_content(); ?>
    </div>
</div>

<div class="container">
    <hr class="styled">
</div>

<div class="container gallery">
<h1 class="kokohead">All by Koko Brands</h1>
    <div class="col-md-4">
    <a href="<?php echo get_post_meta($post->ID, "meta-box-brand-link1", true); ?>">
        <img src="<?php echo get_post_meta($post->ID, "meta-box-brand-img1", true); ?>" class="img-responsive">
        <h3 class="kokohead"><?php echo get_post_meta($post->ID, "meta-box-brand-title1", true);  ?></h3>
    </a>
    </div>
    <div class="col-md-4">
    <a href="<?php echo get_post_meta($post->ID, "meta-box-brand-link2", true); ?>">
        <img src="<?php echo get_post_meta($post->ID, "meta-box-brand-img2", true); ?>" class="img-responsive">
        <h3 class="kokohead"><?php echo get_post_meta($post->ID, "meta-box-brand-title2", true);  ?></h3>
    </a>
    </div>
    <a href="<?php echo get_post_meta($post->ID, "meta-box-brand-link3", true); ?>">
    <div class="col-md-4">
    <img src="<?php echo get_post_meta($post->ID, "meta-box-brand-img3", true); ?>" class="img-responsive">
        <h3 class="kokohead"><?php echo get_post_meta($post->ID, "meta-box-brand-title3", true);  ?></h3>
    </div>
    </a>
</div>

<div class="container">
    <hr class="styled">
</div>

<div class="container">
    <div class="form col-md-6 col-xs-11 col-md-offset-3">
    <?php  echo do_shortcode(  get_post_meta($post->ID, "meta-box-contact-shortcode", true) );  ?>

    </div>
</div>
<?php 
			endwhile; // End of the loop.
			?>
<?php
get_footer();
