=== allbykoko ===

Contributors: automattic
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.0
Tested up to: 4.5.3
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A Customized wordpress theme called allbykoko, developed by Olusegun.


=================================================
Contact form 7 code

You can use this code and replicate it to build a contact form
--------------------------------------

<h1 class="kokohead">Drop a line</h1>
<label for="contact-Name"> Your Name (required)</label>
    [text* your-name class:form-control]

<div><label for="email">Email Address</label>
    [email* your-email class:form-control]</div>

<div><label for="phone">Phone Number</label>
[tel* tel-630 class:form-control placeholder "Phone"] </div>

<div><label for="Comments">Comments</label>
    [textarea your-message class:form-control] </div>

<div class="buttonholder">
[submit class:btn class:btn-default "Submit"]
</div>



------------------------------------------------