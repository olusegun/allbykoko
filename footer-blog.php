<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package allbykoko
 */

?>



    <footer id="colophon" class="site-footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
        <div class="site-info" style="text-align:center;">
            <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'koko' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'koko' ), 'WordPress' ); ?></a>
            <span class="sep"> | </span>
            <?php printf( esc_html__( 'Theme Developed: %1$s by %2$s.', 'koko' ), 'koko', '<a href="#" rel="designer">Olusegun</a>' ); ?>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->

<?php wp_footer(); ?>
</body>
</html>